# VueWorkShop_2

1. Creación del proyecto y explciar vue-cli, sus opciones, posibilidades, etc. vue add vuetify, etc.
2. Estructura del proyecto.
3. Descargar otro proyecto para ver tema de menus, cabebecera, dashboard y ya router, vuex, etc.
4.¿Orientarlo a componentes?
5. Crear un componente. props, eventos, etc.


# vue_workshop

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
